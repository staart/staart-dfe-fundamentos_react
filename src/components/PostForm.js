import {useState} from 'react';

//import css
import '../styles/PostForm.css'

//import icons
import userIcon from '../images/user.svg';
import paperPlaneIcon from '../images/paper-plane.svg';

export default function PostForm(props){    
    const [history, setHistory] = useState('');
    const [userName, setUserName] = useState('');

    function handleSubmit(event){
        event.preventDefault();

        props.onSubmit(history, userName);
        
        setHistory('');
        setUserName('');
    }

    return(
        <form className="post-form" onSubmit={handleSubmit}>
            <input placeholder="Escreva uma nova história...." value={history} onChange={(event)=>setHistory(event.target.value)}/>
            <div>
                <img src={userIcon} alt="user"/>
                <input placeholder="Digite seu nome...." value={userName} onChange={(event)=>setUserName(event.target.value)}/>
                <button type="submit"><img src={paperPlaneIcon} alt="Paper plane"/>Publicar</button>
            </div>
        </form>
    )
}